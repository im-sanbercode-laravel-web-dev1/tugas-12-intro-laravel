<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form Sign Up</title>
</head>

<body>

    <h1>Create New Account !</h1>

    <h3>Sign Up Form</h3>

    <form action="/kirim" method="POST">
        @csrf
        <label for="">First Name :</label> <br>
        <input type="text" name="fname"> <br><br>

        <label for="">Last Name :</label> <br>
        <input type="text" name="lname"> <br><br>

        <label for="">Gender</label> <br>
        <input type="radio" name="jk" value="1">Male <br>
        <input type="radio" name="jk" value="2">Female <br><br>

        <label for="">Nationality :</label> <br>
        <select>
            <option>Indonesia</option>
            <option>English</option>
            <option>Other</option>
        </select> <br><br>

        <label for="">Language Spoken :</label> <br>
        <input type="checkbox" name="languagespoken">Bahasa Indonesia <br>
        <input type="checkbox" name="languagespoken">English <br>
        <input type="checkbox" name="languagespoken">Other <br><br>

        <label for="">Bio :</label> <br>
        <textarea name="biodata" cols="30" rows="10"></textarea> <br><br>

        <input type="submit" value="Sign Up">
    </form>

</body>

</html>