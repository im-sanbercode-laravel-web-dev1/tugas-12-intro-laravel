<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function utama()
    {
        return view('welcome');
    }

    public function biodata()
    {
        return view('biodata');
    }

    public function kirim(Request $request)
    {
        $nama1 = $request['fname'];
        $nama2 = $request['lname'];
        $jk = $request['jk'];
        $biodata = $request['biodata'];

        return view('dashboard', ['nama1' => $nama1, 'nama2' => $nama2, 'jk' => $jk, 'biodata' => $biodata]);
    }
}
